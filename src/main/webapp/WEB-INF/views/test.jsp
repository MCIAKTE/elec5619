<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<title>iCart HTML Version</title>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#">首页</a>
				</li>
				<li>
					<a href="#">资料</a>
				</li>
				<li class="disabled">
					<a href="#">信息</a>
				</li>
				<li class="dropdown pull-right">
					 <a href="#" data-toggle="dropdown" class="dropdown-toggle">下拉<strong class="caret"></strong></a>
					<ul class="dropdown-menu">
						<li>
							<a href="#">操作</a>
						</li>
						<li>
							<a href="#">设置栏目</a>
						</li>
						<li>
							<a href="#">更多设置</a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="#">分割线</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4">
			<div class="carousel slide" id="carousel-73741">
				<ol class="carousel-indicators">
					<li class="active" data-slide-to="0" data-target="#carousel-73741">
					</li>
					<li data-slide-to="1" data-target="#carousel-73741">
					</li>
					<li data-slide-to="2" data-target="#carousel-73741">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img alt="" src="img/1.jpg" />
						<div class="carousel-caption">
							<h4>
								棒球
							</h4>
							<p>
								棒球运动是一种以棒打球为主要特点，集体性、对抗性很强的球类运动项目，在美国、日本尤为盛行。
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="img/2.jpg" />
						<div class="carousel-caption">
							<h4>
								冲浪
							</h4>
							<p>
								冲浪是以海浪为动力，利用自身的高超技巧和平衡能力，搏击海浪的一项运动。运动员站立在冲浪板上，或利用腹板、跪板、充气的橡皮垫、划艇、皮艇等驾驭海浪的一项水上运动。
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="img/3.jpg" />
						<div class="carousel-caption">
							<h4>
								自行车
							</h4>
							<p>
								以自行车为工具比赛骑行速度的体育运动。1896年第一届奥林匹克运动会上被列为正式比赛项目。环法赛为最著名的世界自行车锦标赛。
							</p>
						</div>
					</div>
				</div> <a data-slide="prev" href="#carousel-73741" class="left carousel-control">‹</a> <a data-slide="next" href="#carousel-73741" class="right carousel-control">›</a>
			</div>
		</div>
		<div class="span4">
			<ul>
				<li>
					新闻资讯
				</li>
				<li>
					体育竞技
				</li>
				<li>
					娱乐八卦
				</li>
				<li>
					前沿科技
				</li>
				<li>
					环球财经
				</li>
				<li>
					天气预报
				</li>
				<li>
					房产家居
				</li>
				<li>
					网络游戏
				</li>
			</ul>
			<div class="row-fluid">
				<div class="span6">
					 <a id="modal-856246" href="#modal-container-856246" role="button" class="btn" data-toggle="modal">触发遮罩窗体</a>
					
					<div id="modal-container-856246" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h3 id="myModalLabel">
								标题栏
							</h3>
						</div>
						<div class="modal-body">
							<p>
								显示信息
							</p>
						</div>
						<div class="modal-footer">
							 <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button> <button class="btn btn-primary">保存设置</button>
						</div>
					</div>
				</div>
				<div class="span6">
					 <button class="btn" type="button">按钮</button>
				</div>
			</div>
		</div>
		<div class="span4">
			<ul>
				<li>
					新闻资讯
				</li>
				<li>
					体育竞技
				</li>
				<li>
					娱乐八卦
				</li>
				<li>
					前沿科技
				</li>
				<li>
					环球财经
				</li>
				<li>
					天气预报
				</li>
				<li>
					房产家居
				</li>
				<li>
					网络游戏
				</li>
			</ul> <button class="btn btn-block" type="button">按钮</button>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<dl>
				<dt>
					Rolex
				</dt>
				<dd>
					劳力士创始人为汉斯.威尔斯多夫，1908年他在瑞士将劳力士注册为商标。
				</dd>
				<dt>
					Vacheron Constantin
				</dt>
				<dd>
					始创于1775年的江诗丹顿已有250年历史，
				</dd>
				<dd>
					是世界上历史最悠久、延续时间最长的名表之一。
				</dd>
				<dt>
					IWC
				</dt>
				<dd>
					创立于1868年的万国表有“机械表专家”之称。
				</dd>
				<dt>
					Cartier
				</dt>
				<dd>
					卡地亚拥有150多年历史，是法国珠宝金银首饰的制造名家。
				</dd>
			</dl>
		</div>
	</div>
</div>
</body>
</html></html>