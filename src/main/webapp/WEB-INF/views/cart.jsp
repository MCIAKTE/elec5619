<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<link rel="stylesheet" type="text/css" href="/usyd/resources/main.css" />
<title>iCart HTML Version</title>
</head>
<body>
<div class="menu">
<ul>
<li style="width:0px;"><a></a></li>
<li><a href="/usyd/home/${buyerId}" class="home">Home</a></li>

<li class="active"><a href="/usyd/GET/shoppingcarinfo/${buyerId}" class="cart">Transaction History</a></li>

<li style="float:right;"><a href="/usyd/user/signin" class="login">Login</a></li>
<li style="float:right;"><a href="/usyd/user/signup" class="register">Register</a></li>
</ul>
</div>

<div style="width:1000px;margin:0 auto;">
    <div id="cart">

    <div class="content"></div>
  </div>  
	<div id="search">
    <div class="button-search"></div>
        <input type="text" name="filter_name" value="Search"  onkeydown="this.style.color = '#888';" />
      </div>

</div>

<div class="header">
<div class="logo_img"></div></div>

<div id="menu">
<ul>  
<li><a href="categories.html">Desktops</a>
<div>        
<ul>	
<li><a href="default.htm">PC (0)</a></li>
<li><a href="default.htm">Mac (1)</a></li>
</ul>
</div>
</li>
<li><a href="categories.html">Laptops &amp; Notebooks</a>   	
<div>
<ul>	
<li><a href="default.htm">Macs (0)</a></li>
<li><a href="default.htm">Windows (0)</a></li>
</ul>
</div>
</li>
<li><a href="categories.html">Components</a>
<div>
<ul>	
<li><a href="default.htm">Mice and Trackballs (0)</a></li>
<li><a href="default.htm">Monitors (2)</a></li>
<li><a href="default.htm">Printers (0)</a></li>
<li><a href="default.htm">Scanners (0)</a></li>
<li><a href="default.htm">Web Cameras (0)</a></li>
</ul>
</div>
</li>
<li><a href="categories.html">Software</a></li>
<li><a href="categories.html">Phones &amp; PDAs</a></li>
<li><a href="categories.html">Cameras</a></li>
</ul>
</div>




<div style="width:1000px;margin: 0 auto;margin-top:10px;">
<div class="linktree">
        <a href="#">Home</a>
         &raquo; <a href="#">Shopping Cart</a>
    </div>
<h1 style="margin-top:-10px;">Shopping Cart&nbsp;</h1>


<br>
     <form action="#" method="post" id="basket">
      <div class="cart-info">
        <table>

            <tr>
              <td class="remove">Remove</td>
              <td class="image">Image</td>
              <td class="name">Book Name</td>
              <td class="model">Book Price</td>
              <td class="quantity">Transaction Place</td>
              <td class="price">Seller Name</td>
            </tr>

				<c:forEach var="newshoppingcar" items="${shoppingcar}">
				
				
				
				<c:url var="DeleteLink" value="/DELETE/ShoppingCarItems/${buyerId}">
					 	<c:param name="orderID" value="${newshoppingcar.id}"/>
				</c:url>
				

				

				<tr>
				<td><a href="${DeleteLink}">Delete</a></td>
				<td>${newshoppingcar.photo}</td>
				<td>${newshoppingcar.bookName}</td>
				<td>${newshoppingcar.price}</td>
				<td>${newshoppingcar.place}</td>
				<td>${newshoppingcar.sellerName}</td>				
				<td></td>
				
				
				
				</tr>
				</c:forEach>

        </table>
      </div>
    </form>

    <div class="buttons">
      <div class="right"><a href="/usyd/POST/agreeshoppingcar/${buyerId}" class="button"><span>Agree</span></a></div>
      <div class="center"><a href="/usyd/home/${buyerId}" class="button" style="width:120px;"><span>Continue Shopping</span></a></div>
    </div>

</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="icart-footer">
<div class="icart-footer-top">
	<div style="width:1000px; margin:0 auto;">	
	</div>
</div>
<div class="icart-footer-container">
	<div class="column_footer" style="width:350px;">
		<h3>About Us</h3>
		  Zhaohui Chang Shenghui wu Yuying Shao
	</div>
	
	<div class="column_footer">
		<h3>Customer Service</h3>
		<ul>
		<li><a href="#">About Us</a></li>
      		<li><a href="#">Delivery Information</a></li>
      		<li><a href="#">Privacy Policy</a></li>
      		<li><a href="#">Terms &amp; Conditions</a></li>
      		<li><a href="#">Contact Us</a></li>
      		<li><a href="#">Returns</a></li>
      		<li><a href="#">Site Map</a></li>
		</ul>
	</div>
	
	<div class="column_footer" style="width:150px;">
		<h3>Extras</h3>
		<ul>
      		<li><a href="#">Brands</a></li>
      		<li><a href="#">Gift Vouchers</a></li>
      		<li><a href="#">Affiliates</a></li>
      		<li><a href="#">Specials</a></li>
		</ul>
	</div>	
	<div class="column_footer" style="width:150px;">
		<h3>My Account</h3>
		<ul>
      		<li><a href="#">My Account</a></li>
      		<li><a href="#">Order History</a></li>
      		<li><a href="#">Wish List</a></li>
      		<li><a href="#">Newsletter</a></li>
		</ul>
</div>
		
	<div class="column_footer" style="margin-right:0px;">
		<h3>Social</h3>
		<ul class="social">
			<li class="twitter"><a href="../../twitter.com/twitter">Twitter Username</a></li>
			<li class="facebook"><a href="../../facebook.com/Username/default.htm">Facebook</a></li>
			<li class="rss"><a href="#">RSS Feed</a></li>
		</ul>				
	</div>		
	<div class="clearfix"></div>
	</div>		
	<div class="icart-footer-bottom">
		<div style="width:1000px; margin:0 auto;">				
		<div class="icart-logo-footer"><a href="index.php"><img src="images/logo-footer.png" alt="Footer Logo"></a></div>	
			<ul>
			<li style="float:right;padding-top:10px;list-style:none;">&copy; Copyright &copy; 2013.Company name All rights reserved.<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></li>
			</ul>		
		<div class="clearfix"></div>
		</div>

</div>
</div>

</body>
</html>