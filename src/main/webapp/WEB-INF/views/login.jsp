<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- Redirected because we can't set the welcome page to a virtual URL. --%>
<c:redirect url="\usyd\user\signin"/>