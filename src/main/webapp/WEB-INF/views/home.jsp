<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<link rel="stylesheet" type="text/css" href="/usyd/resources/main.css"/>
	<script type="text/javascript" src="/usyd/resources/slider/scripts/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/usyd/resources/slider/jquery.nivo.slider.pack.js"></script>
	<script type="text/javascript">
	    $(window).load(function() {
	        $('#slider').nivoSlider();
	    });
	</script>
	<script src="/usyd/resources/js/main.js"></script>
	<title>iCart HTML Version</title>
</head>
<body>
	<div class="menu">
		<ul>
			<li style="width:0px;"><a></a></li>
			<li class="active"><a href="/usyd/home/${buyerId}" class="home">Home</a></li>

			

			<li style="float:right;"><a href="/usyd/user/signin" class="login">Login</a></li>
			<li style="float:right;"><a href="/usyd/user/signup" class="register">Register</a></li>
		</ul>
	</div>
	<div style="width:1000px;margin:0 auto;">
		<div id="cart">

			<div class="content"></div>
		</div>  
		<div id="search">
			<div class="button-search"></div>
			<input type="text" name="filter_name" value="Search" onclick="this.value = '';" onkeydown="this.style.color = '#888';" />
		</div>
	</div>
	
	<div id="menu">
		<ul>  
			<li><a href="category">Category</a>
				<div>        
					<ul>	
						<li><a href="default.htm" id="category1"></a></li>
						<li><a href="default.htm" id="category2"></a></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
	<div id="wrapper">
		<div class="slider-wrapper theme-orman">
			<div class="ribbon"></div>
			<div id="slider" class="nivoSlider" style="border: 1px solid #444;box-shadow: 0 0 5px 0 #080808;">
				<a href="default.htm"><img src="/usyd/resources/images/books/The Intelligent Investor.jpg" alt="" title="The Intelligent Investor" /></a>
				<a href="default.htm"><img src="/usyd/resources/images/books/Oprah's Aha conversations.jpg" alt="" title="Oprah's Aha conversations" /></a>
			</div><br>
			<div class="controlNav_box"></div>
		</div>
	</div>
	
	<div class="container">
		<div class="text_box_left">Featured</div>
		<div class="box-product">
			<div>
				<div class="showhim">
					<div class="image">
						<a href="/usyd/GET/bookinfo/${buyerId}/${book1_id}"><img src="images/product_holder.jpg" alt="iMac" />
							<div class="showme">
								<div class="description_featured" style="min-height:110px;">	
									<p id="book1_description"></p>
								</div>
							</div>
						</a>
						<div class="name"><a href="#" id="book1_bookName"></a></div>
						<div class="priced">
							<ul>
								<li>$<span id="book1_price"></span></li>
								<li><a href="#">Add to Cart</a></li>
							</ul>
						</div>
						<div style="margin-top:24px;"></div>
					</div>
					<div class="showme"></div>
				</div>           
			</div>            
			<div>
				<div class="showhim">
					<div class="image">
						<a href="/usyd/GET/bookinfo/${buyerId}/${book2_id}"><img src="images/product_holder.jpg" alt="iPhone" />
							<div class="showme">
								<div class="description_featured" style="min-height:110px;">	
									<p id="book2_description"></p>
								</div>
							</div>
						</a>
						<div class="name"><a href="#" id="book2_bookName"></a></div>
						<div class="priced">
							<ul>
								<li>$<span id="book2_price"></span></li>
								<li><a href="#">Add to Cart</a></li>
							</ul>
						</div>
						<div style="margin-top:24px;"></div>
					</div>
				</div>      
			</div>
			<div>
				<div class="showhim">
					<div class="image">
						<a href="/usyd/GET/bookinfo/${buyerId}/${book3_id}"><img src="images/product_holder.jpg" alt="iPod Classic" />
							<div class="showme">
								<div class="description_featured" style="min-height:110px;">	
									<p id="book3_description"></p>
								</div>
							</div>
						</a>
						<div class="name"><a href="#" id="book3_bookName"></a></div>
						<div class="priced">
							<ul>
								<li>$<span id="book3_price"></span></li>
								<li><a href="#">Add to Cart</a></li>
							</ul>
						</div>
						<div style="margin-top:24px;"></div>
					</div>	
					<div class="showme"></div>
				</div>
			</div>
			<div>
				<div class="showhim">
					<div class="image">
						<a href="/usyd/GET/bookinfo/${book4_id}"><img src="images/product_holder.jpg" alt="iPod Nano" />
							<div class="showme">
								<div class="description_featured" style="min-height:110px;">	
									<p id="book4_description"></p>
								</div>
							</div>
						</a>
						<div class="name"><a href="#" id="book4_bookName"></a></div>
						<div class="priced">
							<ul>
								<li>$<span id="book4_price"></span></li>
								<li><a href="#">Add to Cart</a></li>
							</ul>
						</div>
						<div style="margin-top:24px;"></div>
					</div>	
					<div class="showme"></div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="icart-footer">
		<div class="icart-footer-top">
			<div style="width:1000px; margin:0 auto;">	
			</div>
		</div>
		<div class="icart-footer-container">
			<div class="column_footer" style="width:350px;">
				<h3>About Us</h3>
				<div class="footer_description">
					Shenghui Wu<br>
					Zhaohui Chang<br>
					Yuying Shao
				</div>
			</div>
			
			<div class="column_footer">
				<h3>Customer Service</h3>
				<ul>
					<li><a href="#">About Us</a></li>
		      		<li><a href="#">Delivery Information</a></li>
		      		<li><a href="#">Privacy Policy</a></li>
		      		<li><a href="#">Terms &amp; Conditions</a></li>
		      		<li><a href="#">Contact Us</a></li>
		      		<li><a href="#">Returns</a></li>
		      		<li><a href="#">Site Map</a></li>
				</ul>
			</div>
			
			<div class="column_footer" style="width:150px;">
				<h3>Extras</h3>
				<ul>
		      		<li><a href="#">Brands</a></li>
		      		<li><a href="#">Gift Vouchers</a></li>
		      		<li><a href="#">Affiliates</a></li>
		      		<li><a href="#">Specials</a></li>
				</ul>
			</div>
			
			<div class="column_footer" style="width:150px;">
				<h3>My Account</h3>
				<ul>
		      		<li><a href="#">My Account</a></li>
		      		<li><a href="#">Order History</a></li>
		      		<li><a href="#">Wish List</a></li>
		      		<li><a href="#">Newsletter</a></li>
				</ul>
			</div>
				
			<div class="column_footer" style="margin-right:0px;">
				<h3>Social</h3>
				<ul class="social">
					<li class="twitter"><a href="../../twitter.com/twitter">Twitter Username</a></li>
					<li class="facebook"><a href="../../facebook.com/Username/default.htm">Facebook</a></li>
					<li class="rss"><a href="#">RSS Feed</a></li>
				</ul>				
			</div>		
			<div class="clearfix"></div>
		</div>		
			<div class="icart-footer-bottom">
				<div style="width:1000px; margin:0 auto;">				
					<div class="icart-logo-footer"><a href="index.php"><img src="images/logo-footer.png" alt="Footer Logo"></a></div>	
						<ul>
							<li style="float:right;padding-top:10px;list-style:none;"><a href="#">&copy; Copyright &copy; 2013.Company name All rights reserved.<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></li>
						</ul>		
					<div class="clearfix"></div>
				</div>
			</div>
	</div>
</body>
</html>