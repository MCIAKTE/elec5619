package au.edu.usyd.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.usyd.controller.*;
import au.edu.usyd.domain.*;
import au.edu.usyd.dao.BookDao;


/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/")
@Transactional
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	

	@RequestMapping("/start")
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
	
		return "start";
	}
	
	@Autowired
	BookDao bookDao;
	
	@RequestMapping(value = "/adddata", method = RequestMethod.GET)
	public String adddata(Locale locale, Model model) {

    Book book = new Book();
    book.setBookName("mike");
    book.setPrice(5);
    book.setUserId(1);
    book.setDescription("WOW!this is mike");
    book.setUos(5619);
    book.setPlace("Fisher");
    book.setBookState("Good");
    book.setTypeId(1);
    book.setAlbumId(1);
    
    bookDao.saveBook(book);
    return "start";
	}


		
}
