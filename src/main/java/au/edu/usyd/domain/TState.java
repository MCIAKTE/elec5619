package au.edu.usyd.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tstate")
public class TState {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Column(name = "bookid")
    private int bookid;
    
    @Column(name = "sconfirm")
    private String sconfirm;
    
    @Column(name = "bconfirm")
    private String bconfirm;
    
    @Column(name = "ostate")
    private String ostate;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getBookId() {
        return bookid;
    }

    public void setBookId(int bookid) {
        this.bookid = bookid;
    }
    
    public String getSconfirm() {
        return sconfirm;
    }

    public void setSconfirm(String sconfirm) {
        this.sconfirm = sconfirm;
    }
    
    public String getBconfirm() {
        return bconfirm;
    }

    public void setBconfirm(String bconfirm) {
        this.bconfirm = bconfirm;
    }
    
    public String getOstate() {
        return ostate;
    }

    public void setOstate(String ostate) {
        this.ostate = ostate;
    }
    
}
