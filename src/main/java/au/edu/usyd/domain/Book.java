package au.edu.usyd.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
   
    @Column(name = "bookname")
    private String bookname;
    
    @Column(name = "price")
    private double price;
 
    @Column(name = "uos")
    private int uos;

    @Column(name = "description")
    private String description;
    
    @Column(name = "place")
    private String place;
    
    @Column(name = "bookstate")
    private String bookstate;
    
    @Column(name = "userid")
    private int userid;
    
    @Column(name = "typeid")
    private int typeid;
    
    @Column(name = "albumid")
    private int albumid;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookname;
    }

    public void setBookName(String bookname) {
        this.bookname = bookname;
    }
    
    public double getPrice() {
        return price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    
    public int getUserId() {
        return userid;
    }
    
    public void setUserId(int userid) {
        this.userid = userid;
    }
    
    public int getAlbumId() {
        return albumid;
    }
    
    public void setAlbumId(int albumid) {
        this.albumid = albumid;
    }
    
    public int getTypeId() {
        return typeid;
    }

    public void setTypeId(int typeid) {
        this.typeid = typeid;
    }
    
    public int getUos() {
        return uos;
    }

    public void setUos(int uos) {
        this.uos = uos;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    
    public String getBookState() {
        return bookstate;
    }

    public void setBookState(String bookstate) {
        this.bookstate = bookstate;
    }
    
}