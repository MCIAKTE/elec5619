package au.edu.usyd.dao;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.usyd.domain.Book;


@Repository(value = "bookDao")
public class BookDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveBook(Book book) {
        sessionFactory.getCurrentSession().save(book);
    }
    public Book loadBook(int id){
    	Book book = (Book)sessionFactory.getCurrentSession().get(Book.class, id);
    	
        return book;
    }
}