package au.edu.usyd.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shoppingcar")
public class ShoppingCar {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "bookid")
    private int bookid;
    
    @Column(name = "buyerid")
    private int buyerid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookid;
    }

    public void setBookId(int bookid) {
        this.bookid = bookid;
    }
    
    public int getBuyerId() {
        return buyerid;
    }

    public void setBuyerId(int buyerid) {
        this.buyerid = buyerid;
    }
    
}