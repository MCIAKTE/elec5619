package au.edu.usyd.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shoppingcarinfo")
public class ShoppingCarInfo {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
   
    @Column(name = "bookname")
    private String bookname;
    
    @Column(name = "price")
    private double price;

    @Column(name = "place")
    private String place;
    
    @Column(name = "user")
    private int user;
    
    @Column(name = "photo")
    private String photo;
    
    @Column(name = "sellername")
    private String sellername;
    
    @Column(name = "buyerid")
    private int buyerid;
    
    @Column(name = "rating")
    private int rating;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookname;
    }

    public void setBookName(String bookname) {
        this.bookname = bookname;
    }
    
    public double getPrice() {
        return price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public int getUser() {
        return user;
    }
    
    public void setUser(int user) {
        this.user = user;
    }
    
    public String getPhoto() {
        return photo;
    }
    
    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    
    public String getSellerName() {
        return sellername;
    }

    public void setSellerName(String sellername) {
        this.sellername = sellername;
    }

    public int getBuyerId() {
        return buyerid;
    }

    public void setBuyerId(int buyerid) {
        this.buyerid = buyerid;
    }
    
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
    
}
