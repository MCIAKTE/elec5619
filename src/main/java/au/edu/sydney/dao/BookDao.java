package au.edu.usyd.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.usyd.domain.Book;
import au.edu.usyd.domain.ShoppingCar;


@Repository(value = "bookDao")
public class BookDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveBook(Book book) {
        sessionFactory.getCurrentSession().save(book);
    }
    public Book loadBook(int id){
    	Book book = (Book)sessionFactory.getCurrentSession().get(Book.class, id);
        return book;
    }
    
    public void updateBook(Book book){
    	sessionFactory.getCurrentSession().update(book);
    }
   
    public void deleteBook(int bookid){
    	sessionFactory.getCurrentSession().delete(bookid);
    }
    
  //get book list
    public List<Book> loadBookList(){
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("from Book");
    	List<Book> book = query.list();
    	return book;
    }
    
    //get four books
    public List<Book> loadFourBooks(){
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("from Book where bookState = 'on'");
    	query.setMaxResults(4);
    	List<Book> book = query.list();
    	return book;
    }
    
    
}