package au.edu.usyd.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.usyd.domain.User;

@Repository(value = "userDao")
public class UserDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }
    public User loadUser(int id){
    	User user = (User)sessionFactory.getCurrentSession().get(User.class, id);
        return user;
    }
    
    //get user list
    public List<User> loadUserList(){
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("from User");
    	List<User> user = query.list();
    	return user;
    } 
}