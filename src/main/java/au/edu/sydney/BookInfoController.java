package au.edu.sydney;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.dao.BookDao;
import au.edu.sydney.dao.PersonDao;
import au.edu.sydney.domain.Book;
import au.edu.sydney.domain.Person;

@Controller
@RequestMapping("/bookinfo")
@Transactional
public class BookInfoController {
	
	int Id = 0;
	
	private static final Logger logger = LoggerFactory.getLogger(BookInfoController.class);
	
	@Autowired
	BookDao bookDao;
	
	@RequestMapping(value = "/{bookId}", method = RequestMethod.GET)
	public String bookinfo(@PathVariable("bookId")Integer bookId, Locale locale, Model model) {
		
		logger.info("Welcome home! The client locale is {}.", locale);
		    Id = bookId;
		    Book book = bookDao.loadBook(Id);
		    String bookname = book.getBookName();
		    String booktype = book.getType();
		    int lecturenumber =book.getLectureNumber();
		    String description = book.getDescription();
		    
	        model.addAttribute("bookname", bookname);
	        model.addAttribute("booktype", booktype);
	        model.addAttribute("lecturenumber", lecturenumber);
	        model.addAttribute("description", description);
	        
		return "bookinfo";
	}
}