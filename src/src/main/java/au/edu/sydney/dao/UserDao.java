package au.edu.sydney.dao;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Book;
import au.edu.sydney.domain.User;

@Repository(value = "userDao")
public class UserDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }
    public User loadUser(int id){
    	User user = (User)sessionFactory.getCurrentSession().get(User.class, id);
        return user;
    }
    
}