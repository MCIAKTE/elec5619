package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transactionrecord")
public class TransactionRecord {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "saleuserid")
    private int saleuserid;
    
    @Column(name = "bookid")
    private int bookid;
    
    @Column(name = "buyuserid")
    private int buyuserid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSaleUserId() {
        return saleuserid;
    }

    public void setSaleUserId(int saleuserid) {
        this.saleuserid = saleuserid;
    }
    
    public int getBookId() {
        return saleuserid;
    }

    public void setBookId(int bookid) {
        this.bookid = bookid;
    }
    
    public int getBuyUserId() {
        return buyuserid;
    }

    public void setBuyUserId(int buyuserid) {
        this.buyuserid = buyuserid;
    }
    
}