package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "bookname")
    private String bookname;
    
    @Column(name = "userid")
    private int userid;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "lecturenumber")
    private int lecturenumber;

    @Column(name = "description")
    private String description;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookname;
    }

    public void setBookName(String bookname) {
        this.bookname = bookname;
    }
    
    public int getUserId() {
        return userid;
    }
    
    public void setUserId(int userid) {
        this.userid = userid;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public int getLectureNumber() {
        return lecturenumber;
    }

    public void setLectureNumber(int lecturenumber) {
        this.lecturenumber = lecturenumber;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}